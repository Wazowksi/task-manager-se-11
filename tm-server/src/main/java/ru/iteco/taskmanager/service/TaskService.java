package ru.iteco.taskmanager.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import ru.iteco.taskmanager.api.service.ITaskService;
import ru.iteco.taskmanager.entity.Task;
import ru.iteco.taskmanager.repository.TaskRepository;

public final class TaskService extends AbstractService implements ITaskService{

	@NotNull 
	private final TaskRepository taskRepository; 
	
	public TaskService(@NotNull final TaskRepository taskRepository) {
		this.taskRepository = taskRepository;
	}
	
	public void merge(@NotNull final String name, @NotNull final String description, @NotNull final String uuid, @NotNull final String projectUuid, @NotNull final String ownerId, @NotNull final String dateBegin, @NotNull final String dateEnd) {
		if (!name.equals(null) && !description.equals(null) &&  !uuid.equals(null) && !projectUuid.equals(null) && !ownerId.equals(null) && !dateBegin.equals(null) && !dateEnd.equals(null))
			taskRepository.merge(name, description, uuid, projectUuid, ownerId, dateBegin, dateEnd);
	}
	
	public void set(@Nullable final List<Task> list) {
		if (list == null) return;
		taskRepository.clear();
		for (final Task task : list) {
			taskRepository.merge(task.getName(), task.getDescription(), task.getUuid(), task.getProjectUUID(), task.getOwnerId(), task.getDateBegin(), task.getDateEnd());
		}
	}
	
	public void remove(@NotNull final String name) {
		if (!name.equals(null)) {
			taskRepository.removeByName(name);
		}
	}
	
	public void removeAllByProjectUuid(@NotNull final String uuid) {
		if (!uuid.equals(null))
			taskRepository.removeAllByProjectUuid(uuid);
	}
	
	public void removeAll() {
		taskRepository.clear();
	}
	
	@Nullable
	public Task findByUuid(@NotNull final String uuid) {
		if (!uuid.equals(null)) {
			return taskRepository.findByUuid(uuid);
		}
		return null;
	}
	
	@Nullable
	public Task findByName(@NotNull final String name) {
		if (!name.equals(null)) {
			return taskRepository.findByName(name);
		}
		return null;
	}
	
	@Nullable
	public Task findByName(@NotNull final String name, @NotNull final String ownerId) {
		if (!name.equals(null) && !ownerId.equals(null)) {
			return taskRepository.findByName(name, ownerId);
		}
		return null;
	}
	
	@Nullable
	public List<Task> findAll() {
		@Nullable
		final List<Task> tempList = taskRepository.findAll();
		if (tempList.size() == 0)
			return null;
		return tempList;
	}
	
	@Nullable
	public List<Task> findAll(@NotNull final String projectUuid) {
		@Nullable 
		List<Task> resultList = new ArrayList<Task>();
		@Nullable
		final List<Task> tempList = taskRepository.findAll();
		if (tempList.size() == 0)
			return null;
		for (final Task task : tempList) {
			if (task.getProjectUUID().equals(projectUuid))
				resultList.add(task);
		}
		return resultList;
	}
	
	@Nullable
	public List<Task> findAll(@NotNull final String projectUuid, @NotNull final String ownerId) {
		@Nullable 
		List<Task> resultList = new ArrayList<Task>();
		@Nullable
		final List<Task> tempList = taskRepository.findAll();
		if (tempList.size() == 0)
			return null;
		for (final Task task : tempList) {
			if (task.getProjectUUID().equals(projectUuid) && task.getOwnerId().equals(ownerId))
				resultList.add(task);
		}
		return resultList;
	}
	
	@Nullable
	public List<Task> findAllByPartName(@NotNull final String ownerId, @NotNull final String partOfName) {
		if (!ownerId.equals(null) || !partOfName.equals(null)) {
			List<Task> list = taskRepository.findAll(ownerId);
			final Iterator<Task> iterator = list.iterator();
			while (iterator.hasNext()) {
				@Nullable 
				final Task task = iterator.next();
				if (!task.getName().contains(partOfName)) {
					iterator.remove();
				}
			}
			return list;
		}
		return null;
	}
	
	@Nullable
	public List<Task> findAllByPartDescription(@NotNull final String ownerId, @NotNull final String partOfDescription) {
		if (!ownerId.equals(null) || !partOfDescription.equals(null)) {
			List<Task> list = taskRepository.findAll(ownerId);
			final Iterator<Task> iterator = list.iterator();
			while (iterator.hasNext()) {
				@Nullable 
				final Task task = iterator.next();
				if (!task.getDescription().contains(partOfDescription)) {
					iterator.remove();
				}
			}
			return list;
		}
		return null;
	}
}
