package ru.iteco.taskmanager.entity;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.jetbrains.annotations.Nullable;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@JacksonXmlRootElement(localName = "domain")
public class Domain {

	@Nullable
	@XmlElement(name = "project")
	private List<Project> projectList;
	
	@Nullable
	@XmlElement(name = "task")
	private List<Task> taskList;
	
	@Nullable
	@XmlElement(name = "user")
	private List<User> userList;
	
	public Domain() {
		
	}
	
	public Domain(@Nullable List<User> userList, @Nullable List<Project> projectList, @Nullable List<Task> taskList) {
		this.userList = userList;
		this.projectList = projectList;
		this.taskList = taskList;
	}
}
