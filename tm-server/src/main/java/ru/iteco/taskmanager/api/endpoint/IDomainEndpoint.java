package ru.iteco.taskmanager.api.endpoint;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import org.jetbrains.annotations.Nullable;

import ru.iteco.taskmanager.entity.Domain;
import ru.iteco.taskmanager.entity.Session;

@WebService
public interface IDomainEndpoint {

	@WebMethod
	@Nullable
	Domain saveData(
			@WebParam(name = "session") @Nullable final Session session
			) throws Exception;
	
	@WebMethod
	@Nullable
	Domain loadData(
			@WebParam(name = "session") @Nullable final Session session
			) throws Exception;

	@WebMethod
	@Nullable
	Domain jaxbSaveXml(
			@WebParam(name = "session") @Nullable final Session session
			) throws Exception;

	@WebMethod
	@Nullable
	Domain jaxbLoadXml(
			@WebParam(name = "session") @Nullable final Session session
			) throws Exception;

	@WebMethod
	@Nullable
	Domain jaxbSaveJson (
			@WebParam(name = "session") @Nullable final Session session
			)  throws Exception;

	@WebMethod
	@Nullable
	Domain jaxbLoadJson(
			@WebParam(name = "session") @Nullable final Session session
			) throws Exception;

	@WebMethod
	@Nullable
	Domain jacksonSaveXml(
			@WebParam(name = "session") @Nullable final Session session
			) throws Exception;

	@WebMethod
	@Nullable
	Domain jacksonLoadXml(
			@WebParam(name = "session") @Nullable final Session session
			) throws Exception;

	@WebMethod
	@Nullable
	Domain jacksonSaveJson(
			@WebParam(name = "session") @Nullable final Session session
			) throws Exception;

	@WebMethod
	@Nullable
	Domain jacksonLoadJson(
			@WebParam(name = "session") @Nullable final Session session
			) throws Exception;
}
