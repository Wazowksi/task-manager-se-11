package ru.iteco.taskmanager.api.service;

import java.util.List;

import org.jetbrains.annotations.NotNull;

import ru.iteco.taskmanager.entity.Project;

public interface IProjectService {

	void merge(final String name, final String description, final String uuid, final String ownerId, final String dateBegin, final String dateEnd);
	void remove(final String name);
	void removeAll();
	Project findByUuid(String uuid);
	Project findByName(String name);
	List<Project> findAll();
	List<Project> findAllByPartName(@NotNull final String ownerId, @NotNull final String partOfName);
	List<Project> findAllByPartDescription(@NotNull final String ownerId, @NotNull final String partOfDescription);
}
