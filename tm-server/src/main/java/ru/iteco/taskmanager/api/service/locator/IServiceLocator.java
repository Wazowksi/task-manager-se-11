package ru.iteco.taskmanager.api.service.locator;

import ru.iteco.taskmanager.service.DomainService;
import ru.iteco.taskmanager.service.ProjectService;
import ru.iteco.taskmanager.service.SessionService;
import ru.iteco.taskmanager.service.TaskService;
import ru.iteco.taskmanager.service.UserService;

public interface IServiceLocator {

	ProjectService getProjectService();
	TaskService getTaskService();
	UserService getUserService();
	DomainService getDomainService();
	SessionService getSessionService();
}
