package ru.iteco.taskmanager.enumerate;

import org.jetbrains.annotations.NotNull;

import lombok.Getter;

public enum RoleType {

	ADMIN ("Administrator"),
	USER ("User");
	
	@NotNull
	@Getter
	private String displayName;

	RoleType(final String displayName) {
		this.displayName = displayName;
	}
	
	@NotNull
	@Override
	public String toString() {
		return displayName;
	}
}
