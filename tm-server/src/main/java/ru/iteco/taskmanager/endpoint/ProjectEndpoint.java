package ru.iteco.taskmanager.endpoint;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import org.jetbrains.annotations.Nullable;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.iteco.taskmanager.api.endpoint.IProjectEndpoint;
import ru.iteco.taskmanager.api.service.locator.IServiceLocator;
import ru.iteco.taskmanager.entity.Project;
import ru.iteco.taskmanager.entity.Session;
import ru.iteco.taskmanager.util.SignatureUtil;

@Getter
@Setter
@NoArgsConstructor
@WebService(endpointInterface = "ru.iteco.taskmanager.api.endpoint.IProjectEndpoint")
public class ProjectEndpoint implements IProjectEndpoint {
	
	private IServiceLocator serviceLocator;
	
	@WebMethod
	public void projectMerge(
			@WebParam(name = "session") @Nullable final Session session,
			@WebParam(name = "name") @Nullable final String projectName, 
			@WebParam(name = "description") @Nullable final String description, 
			@WebParam(name = "uuid") @Nullable final String uuid, 
			@WebParam(name = "ownerId") @Nullable final String ownerId,
			@WebParam(name = "dateBegin") @Nullable final String dateBegin, 
			@WebParam(name = "dateEnd") @Nullable final String dateEnd
			) {
		if (SignatureUtil.validate(session) == null) return;
		serviceLocator.getProjectService().merge(projectName, description, uuid, ownerId, dateBegin, dateEnd);
	}

	@WebMethod
	public void projectSet(
			@WebParam(name = "session") @Nullable final Session session,
			@WebParam(name = "projectList") @Nullable List<Project> list
			) {
		if (SignatureUtil.validate(session) == null) return;
		serviceLocator.getProjectService().set(list);
	}

	@WebMethod
	public void removeByUuid(
			@WebParam(name = "session") @Nullable final Session session,
			@WebParam(name = "uuid") @Nullable String uuid
			) {
		if (SignatureUtil.validate(session) == null) return;
		serviceLocator.getProjectService().remove(uuid);
	}

	@WebMethod
	public void removeByUuidAndOwnerId(
			@WebParam(name = "session") @Nullable final Session session,
			@WebParam(name = "uuid") @Nullable String uuid, 
			@WebParam(name = "ownerId") @Nullable String ownerId
			) {
		if (SignatureUtil.validate(session) == null) return;
		serviceLocator.getProjectService().remove(uuid, ownerId);
	}

	@WebMethod
	public void removeAll(
			@WebParam(name = "session") @Nullable final Session session
			) {
		if (SignatureUtil.validate(session) == null) return;
		serviceLocator.getProjectService().removeAll();
	}

	@WebMethod
	public @Nullable Project findByUuid(
			@WebParam(name = "session") @Nullable final Session session,
			@WebParam(name = "uuid") @Nullable String uuid
			) {
		return serviceLocator.getProjectService().findByUuid(uuid);
	}

	@WebMethod
	public @Nullable Project findByUuidAndOwnerId(
			@WebParam(name = "session") @Nullable final Session session,
			@WebParam(name = "uuid") @Nullable String uuid, 
			@WebParam(name = "ownerId") @Nullable String ownerId
			) {
		SignatureUtil.validate(session);
		return serviceLocator.getProjectService().findByUuid(uuid, ownerId);
	}

	@WebMethod
	public @Nullable Project findProjectByName(
			@WebParam(name = "session") @Nullable final Session session,
			@WebParam(name = "projectName") @Nullable String name
			) {
		SignatureUtil.validate(session);
		return serviceLocator.getProjectService().findByName(name);
	}

	@WebMethod
	public @Nullable Project findProjectByNameAndOwnerId(
			@WebParam(name = "session") @Nullable final Session session,
			@WebParam(name = "projectName") @Nullable String name, 
			@WebParam(name = "ownerId") @Nullable String ownerId
			) {
		SignatureUtil.validate(session);
		return serviceLocator.getProjectService().findByName(name, ownerId);
	}

	@WebMethod
	public @Nullable List<Project> findAllProject(
			@WebParam(name = "session") @Nullable final Session session
			) {
		SignatureUtil.validate(session);
		return serviceLocator.getProjectService().findAll();
	}

	@WebMethod
	public @Nullable List<Project> findAllByOwnerId(
			@WebParam(name = "session") @Nullable final Session session,
			@WebParam(name = "ownerId") @Nullable String ownerId
			) {
		SignatureUtil.validate(session);
		return serviceLocator.getProjectService().findAll(ownerId);
	}

	@WebMethod
	public @Nullable List<Project> findAllProjectByPartName(
			@WebParam(name = "session") @Nullable final Session session,
			@WebParam(name = "ownerId") @Nullable String ownerId, 
			@WebParam(name = "partOfName") @Nullable String partOfName
			) {
		SignatureUtil.validate(session);
		return serviceLocator.getProjectService().findAllByPartName(ownerId, partOfName);
	}

	@WebMethod
	public @Nullable List<Project> findAllProjectByPartDescription(
			@WebParam(name = "session") @Nullable final Session session,
			@WebParam(name = "ownerId") @Nullable String ownerId, 
			@WebParam(name = "partOfDescription") @Nullable String partOfDescription
			) {
		SignatureUtil.validate(session);
		return serviceLocator.getProjectService().findAllByPartDescription(ownerId, partOfDescription);
	}
}
