# Task Manager SE-11

This is another implementation of the Task Manager project

## Software

Work OS and clean hands, actual JDK

## Stack

Java 1.7, Maven

## Deploy

mvn clean install

## Launch

java -jar "task-manager-server.jar"
java -jar "task-manager-client.jar"

## Contacts

Denis Ermakov

