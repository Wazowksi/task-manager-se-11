package ru.iteco.taskmanager.enumerate;

import org.jetbrains.annotations.NotNull;

import lombok.Getter;

public enum ReadinessStatus {

	PLANNED ("Planned"),
	DURING ("During"),
	READY ("Ready");
	
	@NotNull
	@Getter
	private String displayName;

	ReadinessStatus(final String displayName) {
		this.displayName = displayName;
	}
	
	@NotNull
	@Override
	public String toString() {
		return displayName;
	}
}
