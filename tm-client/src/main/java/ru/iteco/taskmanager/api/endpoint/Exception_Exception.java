
package ru.iteco.taskmanager.api.endpoint;

import javax.xml.ws.WebFault;


/**
 * This class was generated by Apache CXF 3.2.7
 * 2019-11-15T17:34:53.117+03:00
 * Generated source version: 3.2.7
 */

@WebFault(name = "Exception", targetNamespace = "http://endpoint.api.taskmanager.iteco.ru/")
public class Exception_Exception extends java.lang.Exception {

    private ru.iteco.taskmanager.api.endpoint.Exception exception;

    public Exception_Exception() {
        super();
    }

    public Exception_Exception(String message) {
        super(message);
    }

    public Exception_Exception(String message, java.lang.Throwable cause) {
        super(message, cause);
    }

    public Exception_Exception(String message, ru.iteco.taskmanager.api.endpoint.Exception exception) {
        super(message);
        this.exception = exception;
    }

    public Exception_Exception(String message, ru.iteco.taskmanager.api.endpoint.Exception exception, java.lang.Throwable cause) {
        super(message, cause);
        this.exception = exception;
    }

    public ru.iteco.taskmanager.api.endpoint.Exception getFaultInfo() {
        return this.exception;
    }
}
