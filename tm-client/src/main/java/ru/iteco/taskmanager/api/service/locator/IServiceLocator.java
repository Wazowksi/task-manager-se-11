package ru.iteco.taskmanager.api.service.locator;

import ru.iteco.taskmanager.endpoint.DomainEndpointService;
import ru.iteco.taskmanager.endpoint.ProjectEndpointService;
import ru.iteco.taskmanager.endpoint.SessionEndpointService;
import ru.iteco.taskmanager.endpoint.TaskEndpointService;
import ru.iteco.taskmanager.endpoint.UserEndpointService;
import ru.iteco.taskmanager.service.SessionService;
import ru.iteco.taskmanager.service.TerminalService;

public interface IServiceLocator {

	TerminalService getTerminalService();
	SessionService getSessionService();
	ProjectEndpointService getProjectEndpointService();
    TaskEndpointService getTaskEndpointService();
    UserEndpointService getUserEndpointService();
    DomainEndpointService getDomainEndpointService();
    SessionEndpointService getSessionEndpointService();
}
