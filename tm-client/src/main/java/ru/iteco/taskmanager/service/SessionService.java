package ru.iteco.taskmanager.service;

import org.jetbrains.annotations.Nullable;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.iteco.taskmanager.api.endpoint.Session;

@Getter
@Setter
@NoArgsConstructor
public final class SessionService extends AbstractService{

	@Nullable
	private Session session = null;
}
