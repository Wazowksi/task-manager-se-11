package ru.iteco.taskmanager.command.serialize.save;

import org.jetbrains.annotations.Nullable;

import ru.iteco.taskmanager.api.endpoint.Session;
import ru.iteco.taskmanager.command.AbstractCommand;

public class DataToXmlJacksonCommand extends AbstractCommand {

	@Override
	public String command() {
		return "jackson-to-xml";
	}

	@Override
	public String description() {
		return "  -  save data to xml via jackson";
	}

	@Override
	public void execute() throws Exception {
		@Nullable
		final Session session = serviceLocator.getSessionService().getSession();
		if (session == null) return;
		
		if (serviceLocator.getDomainEndpointService().getDomainEndpointPort().jacksonSaveXml(session) != null) {
			serviceLocator.getSessionService().setSession(null);
            System.out.println("Done");
		}
	}

}
