package ru.iteco.taskmanager.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import ru.iteco.taskmanager.api.endpoint.ISessionEndpoint;
import ru.iteco.taskmanager.api.endpoint.Session;
import ru.iteco.taskmanager.command.AbstractCommand;

public final class UserLogoutCommand extends AbstractCommand{
	
	@Override
	public String command() {
		return "logout";
	}

	@Override
	public String description() {
		return "  -  user logout";
	}

	@Override
	public void execute() throws Exception {
		@NotNull
		final ISessionEndpoint sessionEndpoint = serviceLocator.getSessionEndpointService().getSessionEndpointPort();
		@Nullable 
        final Session session = serviceLocator.getSessionService().getSession();
        if (session == null) return;
		sessionEndpoint.removeSession(session);
		serviceLocator.getSessionService().setSession(null);
		System.out.println("Done");
		serviceLocator.getTerminalService().get("login").execute();
	}

}
