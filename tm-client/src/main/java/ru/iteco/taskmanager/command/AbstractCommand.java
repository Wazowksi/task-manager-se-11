package ru.iteco.taskmanager.command;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.jetbrains.annotations.NotNull;

import lombok.Getter;
import lombok.Setter;
import ru.iteco.taskmanager.api.service.locator.IServiceLocator;

@Getter
@Setter
public abstract class AbstractCommand {

	protected IServiceLocator serviceLocator;
	protected Scanner scanner;
	
	public AbstractCommand() {
		scanner = new Scanner(System.in);
	}

	@NotNull
	public abstract String command();
	@NotNull
	public abstract String description();
	public abstract void execute() throws Exception;
	
	@NotNull
	public List<String> getRoles() {
		return new ArrayList<String>() { {
			add("User");
			add("Administrator");
		}
		};
	}
}
