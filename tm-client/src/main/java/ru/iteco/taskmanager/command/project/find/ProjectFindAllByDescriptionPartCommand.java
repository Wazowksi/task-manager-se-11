package ru.iteco.taskmanager.command.project.find;

import java.util.List;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import ru.iteco.taskmanager.api.endpoint.IProjectEndpoint;
import ru.iteco.taskmanager.api.endpoint.IUserEndpoint;
import ru.iteco.taskmanager.api.endpoint.Project;
import ru.iteco.taskmanager.api.endpoint.Session;
import ru.iteco.taskmanager.api.endpoint.User;
import ru.iteco.taskmanager.command.AbstractCommand;

public class ProjectFindAllByDescriptionPartCommand extends AbstractCommand{

	@Override
	public String command() {
		return "project-find-all-description-part";
	}

	@Override
	public String description() {
		return "  -  find all project by part of description";
	}

	@Override
	public void execute() throws Exception {
		@NotNull 
		final IUserEndpoint userEndpoint = serviceLocator.getUserEndpointService().getUserEndpointPort();
		@NotNull 
		final IProjectEndpoint projectEndpoint = serviceLocator.getProjectEndpointService().getProjectEndpointPort();
        @Nullable 
        final Session session = serviceLocator.getSessionService().getSession();
        if (session == null) return;
        @Nullable final User user = userEndpoint.getById(session, session.getUserId());
        if (user == null) return;
		
        System.out.print("Part of description of project: ");
		@NotNull
		final String partOfDescription = scanner.nextLine();
		@Nullable
		final List<Project> projectList = projectEndpoint.findAllProjectByPartDescription(session, user.getUuid(), partOfDescription);
		if (projectList == null) throw new Exception("Empty");
		for (int i = 0, j = 1; i < projectList.size(); i++) {
			if (projectList.get(i).getOwnerId().equals(user.getUuid())) {
				System.out.println("[Project " + (j++) + "]");
				System.out.println("UUID: " + projectList.get(i).getUuid());
				System.out.println("Name: " + projectList.get(i).getName());
				System.out.println("Description: " + projectList.get(i).getDescription());
				System.out.println("DateCreated: " + projectList.get(i).getDateCreated());
				System.out.println("DateBegin: " + projectList.get(i).getDateBegin());
				System.out.println("DateEnd: " + projectList.get(i).getDateEnd());
				System.out.println("Status: " + projectList.get(i).getReadinessStatus().toString());
			}
		}
	}

}
