package ru.iteco.taskmanager.command.user;

import java.util.ArrayList;
import java.util.List;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import ru.iteco.taskmanager.api.endpoint.IUserEndpoint;
import ru.iteco.taskmanager.api.endpoint.Session;
import ru.iteco.taskmanager.command.AbstractCommand;

public final class UserCreateCommand extends AbstractCommand {
	
	@Override
	public String command() {
		return "user-create";
	}

	@Override
	public String description() {
		return "  -  create new user";
	}

	@Override
	public void execute() throws Exception {
		@NotNull 
		final IUserEndpoint userEndpoint = serviceLocator.getUserEndpointService().getUserEndpointPort();
        @Nullable 
        final Session session = serviceLocator.getSessionService().getSession();
        if (session == null) return;
		
		System.out.print("Username: ");
		@NotNull 
		final String login = scanner.nextLine();
		if (userEndpoint.get(session, login) != null) {
			System.out.println("Username exist");
			return;
		}
		
		System.out.print("Password: ");
		@NotNull 
		final String password = scanner.nextLine();
		userEndpoint.mergeDefault(session, login, password);
		System.out.println("Done");
	}

	@NotNull
	@Override
	public List<String> getRoles() {
		return new ArrayList<String>() { {
			add("Administrator");
		}
		};
	}
}
