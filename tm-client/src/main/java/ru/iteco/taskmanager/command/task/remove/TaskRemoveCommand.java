package ru.iteco.taskmanager.command.task.remove;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import ru.iteco.taskmanager.api.endpoint.ITaskEndpoint;
import ru.iteco.taskmanager.api.endpoint.IUserEndpoint;
import ru.iteco.taskmanager.api.endpoint.Session;
import ru.iteco.taskmanager.api.endpoint.Task;
import ru.iteco.taskmanager.api.endpoint.User;
import ru.iteco.taskmanager.command.AbstractCommand;

public final class TaskRemoveCommand extends AbstractCommand {

	@Override
	public String command() {
		return "task-remove";
	}

	@Override
	public String description() {
		return "  -  remove task from project";
	}

	@Override
	public void execute() throws Exception {
		@NotNull 
		final IUserEndpoint userEndpoint = serviceLocator.getUserEndpointService().getUserEndpointPort();
		@NotNull 
		final ITaskEndpoint taskEndpoint = serviceLocator.getTaskEndpointService().getTaskEndpointPort();
        @Nullable 
        final Session session = serviceLocator.getSessionService().getSession();
        if (session == null) return;
        @Nullable final User user = userEndpoint.getById(session, session.getUserId());
        if (user == null) return;
		
		System.out.print("Name of task: ");
		@NotNull
		final String inputName = scanner.nextLine();
		@Nullable
		final Task tempTask = taskEndpoint.findByName(session, inputName);
		if (tempTask == null) throw new Exception("No task with same name");
		if (!tempTask.getOwnerId().equals(user.getUuid())) {
			System.out.println("You don't have permission");
			return;
		}
		
		taskEndpoint.remove(session, inputName);
		System.out.println("Done");
	}
}
