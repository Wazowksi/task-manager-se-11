package ru.iteco.taskmanager.command.project.find;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import ru.iteco.taskmanager.api.endpoint.IProjectEndpoint;
import ru.iteco.taskmanager.api.endpoint.IUserEndpoint;
import ru.iteco.taskmanager.api.endpoint.Project;
import ru.iteco.taskmanager.api.endpoint.Session;
import ru.iteco.taskmanager.api.endpoint.User;
import ru.iteco.taskmanager.command.AbstractCommand;

public final class ProjectFindCommand extends AbstractCommand {
	
	@Override
	public String command() {
		return "project-find";
	}

	@Override
	public String description() {
		return "  -  find one project";
	}

	@Override
	public void execute() throws Exception {
		@NotNull 
		final IUserEndpoint userEndpoint = serviceLocator.getUserEndpointService().getUserEndpointPort();
		@NotNull 
		final IProjectEndpoint projectEndpoint = serviceLocator.getProjectEndpointService().getProjectEndpointPort();
        @Nullable 
        final Session session = serviceLocator.getSessionService().getSession();
        if (session == null) return;
        @Nullable final User user = userEndpoint.getById(session, session.getUserId());
        if (user == null) return;
		
		System.out.print("Name of project: ");
		@NotNull
		final String inputName = scanner.nextLine();
		@Nullable
		final Project tempProject = projectEndpoint.findProjectByName(session, inputName);
		
		if (tempProject == null) throw new Exception("No project with same name");
		System.out.println("UUID: " + tempProject.getUuid());
		System.out.println("Name: " + tempProject.getName());
		System.out.println("Description: " + tempProject.getDescription());
		System.out.println("DateCreated: " + tempProject.getDateCreated());
		System.out.println("DateBegin: " + tempProject.getDateBegin());
		System.out.println("DateEnd: " + tempProject.getDateEnd());
		System.out.println("Status: " + tempProject.getReadinessStatus().toString());
	}
}
