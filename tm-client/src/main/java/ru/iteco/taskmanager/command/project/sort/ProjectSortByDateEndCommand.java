package ru.iteco.taskmanager.command.project.sort;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import ru.iteco.taskmanager.api.endpoint.IProjectEndpoint;
import ru.iteco.taskmanager.api.endpoint.IUserEndpoint;
import ru.iteco.taskmanager.api.endpoint.Project;
import ru.iteco.taskmanager.api.endpoint.Session;
import ru.iteco.taskmanager.api.endpoint.User;
import ru.iteco.taskmanager.command.AbstractCommand;

public class ProjectSortByDateEndCommand extends AbstractCommand {

	@Override
	public String command() {
		return "project-sort-date-end";
	}

	@Override
	public String description() {
		return "  -  find all project and sort them by date end";
	}

	@Override
	public void execute() throws Exception {
		@NotNull 
		final IUserEndpoint userEndpoint = serviceLocator.getUserEndpointService().getUserEndpointPort();
		@NotNull 
		final IProjectEndpoint projectEndpoint = serviceLocator.getProjectEndpointService().getProjectEndpointPort();
        @Nullable 
        final Session session = serviceLocator.getSessionService().getSession();
        if (session == null) return;
        @Nullable final User user = userEndpoint.getById(session, session.getUserId());
        if (user == null) return;
		
		@Nullable
		final List<Project> tempList = projectEndpoint.findAllProject(session);
		
		@NotNull
		final Comparator<Project> compareByDateEnd = (Project o1, Project o2) -> o1.getDateEnd().compareTo(o2.getDateEnd() );
		Collections.sort(tempList, compareByDateEnd);
		
		for (int i = 0, j = 1; i < tempList.size(); i++) {
			if (tempList.get(i).getOwnerId().equals(user.getUuid())) {
				System.out.println("[Project " + (j++) + "]");
				System.out.println(tempList.get(i));
			}
		}
	}
}
