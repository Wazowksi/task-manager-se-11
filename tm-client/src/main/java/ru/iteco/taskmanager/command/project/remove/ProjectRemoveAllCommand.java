package ru.iteco.taskmanager.command.project.remove;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import ru.iteco.taskmanager.api.endpoint.IProjectEndpoint;
import ru.iteco.taskmanager.api.endpoint.Session;
import ru.iteco.taskmanager.command.AbstractCommand;

public final class ProjectRemoveAllCommand extends AbstractCommand{

	@Override
	public String command() {
		return "project-remove";
	}

	@Override
	public String description() {
		return "  -  remove one project";
	}

	@Override
	public void execute() throws Exception {
		@NotNull 
		final IProjectEndpoint projectEndpoint = serviceLocator.getProjectEndpointService().getProjectEndpointPort();
        @Nullable 
        final Session session = serviceLocator.getSessionService().getSession();
        if (session == null) return;
		projectEndpoint.removeAll(session);
		System.out.println("Done");
	}
}
