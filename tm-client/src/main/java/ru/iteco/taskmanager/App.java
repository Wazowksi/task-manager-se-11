package ru.iteco.taskmanager;

import org.jetbrains.annotations.NotNull;

import ru.iteco.taskmanager.bootstrap.Bootstrap;

public class App
{
	public static void main(final String[] args )
    {
    	@NotNull
    	final Bootstrap bootstrap = new Bootstrap();
    	bootstrap.init();
    }
}
