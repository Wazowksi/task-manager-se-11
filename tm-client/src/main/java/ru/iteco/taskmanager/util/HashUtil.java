package ru.iteco.taskmanager.util;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class HashUtil {

	@Nullable
    public static String getHash(@NotNull final String value) {
        try {
            @NotNull MessageDigest digest = MessageDigest.getInstance("MD5");
            byte[] messageDigest =  digest.digest(value.getBytes(StandardCharsets.UTF_8));

            StringBuilder hexString = new StringBuilder();
            for (int i = 0; i < messageDigest.length; i++) {
                String h = Integer.toHexString(0xFF & messageDigest[i]);
                while (h.length() < 2)
                    h += "0";
                hexString.append(h);
            }

            return hexString.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
